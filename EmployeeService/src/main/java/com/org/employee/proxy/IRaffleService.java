package com.org.employee.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.org.employee.model.RequestReleaseBean;
import com.org.employee.model.RequestSpotBean;

@FeignClient(name="RAFFLE-SERVICE")
@RibbonClient(name="RAFFLE-SERVICE")
public interface IRaffleService {
	
	@PostMapping("/raffle/spotreleasemanagement")
	public String releaseSpot(@RequestBody RequestReleaseBean reqbean);

	@PostMapping("/raffle/spotrequestmanagement")
	public String requestSpot(@RequestBody RequestSpotBean reqbean);		

}
