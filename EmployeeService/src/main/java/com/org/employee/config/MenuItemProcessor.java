package com.org.employee.config;

import org.springframework.batch.item.ItemProcessor;

import com.org.employee.model.Employee;

public class MenuItemProcessor implements ItemProcessor<Employee ,Employee>{
	
	@Override
	  public Employee process(final Employee Employee) throws Exception {
		final String name = Employee.getName();
		final long contactnumber = Employee.getContactnumber();
		final int experience = Employee.getexperience();
	    final String designation = Employee.getDesignation();	    

	    final Employee processedItem = new Employee(1L,name,contactnumber,experience,designation);
	    return processedItem;
	  }

}
