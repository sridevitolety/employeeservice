package com.org.employee.model;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class SpotIdBean {
	
	private long empid;	
	private List<Date> releasefordates;
	
	
	public List<Date> getReleasefordates() {
		return releasefordates;
	}

	public void setReleasefordates(List<Date> releasefordates) {
		this.releasefordates = releasefordates;
	}

	public long getEmpid() {
		return empid;
	}

	public void setEmpid(long empid) {
		this.empid = empid;
	}

	public SpotIdBean(long empid, List<Date> releasefordates) {
		super();
		this.empid = empid;
		this.releasefordates = releasefordates;
	}
	public SpotIdBean() {}
	
}
