package com.org.employee.model;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RequestReleaseBean {
	
	private long spotid;
	private List<Date> releasefordates;	
	
	public List<Date> getReleasefordates() {
		return releasefordates;
	}

	public void setReleasefordates(List<Date> releasefordates) {
		this.releasefordates = releasefordates;
	}

	public RequestReleaseBean(long spotid, List<Date> releasefordates) {
		super();
		this.spotid = spotid;
		this.releasefordates = releasefordates;
	}

	public long getSpotid() {
		return spotid;
	}

	public void setSpotid(long spotid) {
		this.spotid = spotid;
	}
	public RequestReleaseBean() {}
		
}
