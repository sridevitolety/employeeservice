package com.org.employee.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Employee {

	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	private long contactnumber;
	private int experience;
	private String designation;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(long contactnumber) {
		this.contactnumber = contactnumber;
	}
	public int getexperience() {
		return experience;
	}
	public void setexperience(int experience) {
		this.experience = experience;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}	
	
	public Employee(long id, String name, long contactnumber, int experience, String designation) {
		super();
		this.id = id;
		this.name = name;
		this.contactnumber = contactnumber;
		this.experience = experience;
		this.designation = designation;
	}
	public Employee() {}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", contactnumber=" + contactnumber + ", experience=" + experience
				+ ", designation=" + designation + "]";
	}
	
	
}
