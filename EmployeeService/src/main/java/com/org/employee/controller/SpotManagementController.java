package com.org.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.employee.model.SpotIdBean;
import com.org.employee.model.SpotRequestBean;
import com.org.employee.service.SpotManagementService;

@RestController
@RequestMapping("/employee")
public class SpotManagementController {
	
	@Autowired
	SpotManagementService smServ;
	
	@PostMapping("/spotreleasemanagement")
	public String releaseSpot(@RequestBody SpotIdBean reqBean) {		
		return smServ.releaseSpot(reqBean);
	}
	
	@PostMapping("/spotrequestmanagement")
	public String requestSpot(@RequestBody SpotRequestBean reqbean) {
		return smServ.requestSpot(reqbean);
	}	

}
