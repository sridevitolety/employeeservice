package com.org.employee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.employee.model.RequestReleaseBean;
import com.org.employee.model.SpotIdBean;
import com.org.employee.model.SpotRequestBean;
import com.org.employee.proxy.IRaffleService;
import com.org.employee.proxy.ISpotService;

@Service
public class SpotManagementService {
	
	@Autowired
	IRaffleService rServ;
	
	@Autowired
	ISpotService sServ;

	public String releaseSpot(SpotIdBean reqBean) {
		long spotId = sServ.getspotByEmpId(reqBean.getEmpid());
		RequestReleaseBean rbean = new RequestReleaseBean();
		rbean.setSpotid(spotId);
		rbean.setReleasefordates(reqBean.getReleasefordates());
		return rServ.releaseSpot(rbean);		
	}

	public String requestSpot(SpotRequestBean reqBean) {
		return rServ.requestSpot(reqBean);
	}

}
