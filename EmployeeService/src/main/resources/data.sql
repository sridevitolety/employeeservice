DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
id INT AUTO_INCREMENT PRIMARY KEY,
name varchar(250) not null,
contactnumber number NOT NULL UNIQUE,
experience number NOT NULL,
designation varchar(100) not null
);


